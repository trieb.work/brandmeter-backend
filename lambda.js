// this is the entrypoint for this serverless script. You call it with a POST request and it will start
// pulling data for certain accounts and pages out, and pushes them to Elasticsearch.
import management from './lib/auth0management'
import { find } from 'lodash'
import getFacebook from './adapters/facebook'
import getInstagram from './adapters/instagram'
import getGoogleMybusiness from './adapters/googlemybusiness'
import getLinkedin from './adapters/linkedin'

import * as Sentry from '@sentry/node';
Sentry.init({ dsn: process.env.SENTRY_DSN });

function sentryHandler(lambdaHandler) {
    return async event => {
      try {
        return await lambdaHandler(event);
      } catch (e) {
        Sentry.captureException(e);
        await Sentry.flush(2000);
        return e;
      }
    };
}

export const handler = sentryHandler(async event => {

    const body = event.body
    const query = event.queryStringParameters

    if(!query || !query['user_id'] || !query['page']){
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: 'You are sending wrong values. Sorry mate'
            })
        }
    }

    // get the user identity from auth0, if we don't have it already in the body of the request
    // we filter the identity for the user_id that we got from the request querystring
    const user = await management.getUser({
        id: query['user_id']
    })
    const identity = find(user.identities, {
        'user_id': query['page_account_id']
    })
 
    // Stuff like google mybusiness needs an account ID corresponding to the current page we want to catch metrics
    //user['page_account_id'] = user.app_metadata.pages[query.type][query.page].user_id
    user['page_account_id'] = query['page_account_id']

    



    // start the right adapter depending on the page type
    // needs the page, the user & identity object and the amount of days we want to pull metrics out
    let res = ''
    switch (query.type) {
        case 'linkedin':
            console.log('Started metric job for Linkedin with user', user.name)
            res = await getLinkedin(query.page, user, identity, query.days || 425, query.brand_id)
            console.log('Finished metric job for Linkedin with user', user.name)

            break

        case 'googlemybusiness':
            res = await getGoogleMybusiness(query.page, user, identity, query.days ||460, query.brand_id)
            console.log('Finished metric job for GoogleMyBusiness with user', user.name)
            break
        case 'facebook':
            res = await getFacebook(query.page, user, identity, query.days || 560, query.brand_id)
            console.log('Finished metric job for Facebook with user', user.name)
            break
        case 'instagram':
            res = await getInstagram(query.page, user, identity, query.days || 560, query.brand_id)
            console.log('Finished metric job for Instagram with user', user.name)
            break
    }

    if (res.statusCode === 200){
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: 'ETL job was successful'
            })
        }
    } else {
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'ETL job was not successul!'
            })
        }

    }



})