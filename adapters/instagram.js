import getMetric from '../lib/fbgraph'
import elasticBody from '../lib/elasticBody'
import client from '../lib/elasticConnect'
import graphget from '../lib/fbgraphHelper'
import instagramMedia from '../lib/fbgraphMedia'



export default async function getInstagram(page, user, identity, days, brand_id) {

    const internal_type_mapping = {
        instagram:{
            //no aggregation
            
            //total_actions
            website_clicks: "website_clicks",
            email_contacts: "email_contacts",
            get_directions_clicks: "get_directions_clicks",
            phone_call_clicks: "phone_call_clicks",
            text_message_clicks: "text_message_clicks",
            profile_views: "profile_views",

            //impressions_unique
            impressions: "impressions",

            reach: "engaged_users"

            
        },
    }
    const aggregation_type_mapping = {
        instagram:{
            website_clicks: "total_actions",
            email_contacts: "total_actions",
            get_directions_clicks: "total_actions",
            phone_call_clicks: "total_actions",
            text_message_clicks: "total_actions",
            profile_views: "total_actions",

            impressions: "impressions",

            followers_count: "follower_count",
            
            reach: "engaged_users"

        },
    }


    const instagram_data = await getMetric(page, identity.access_token, {  //ig id
        //day, week, days_28, month, lifetime
        day: {
            metric: Object.keys(internal_type_mapping.instagram),
            scroll:{
                start:1,
                end:days,
                scroll:30,
            }
        },
        lifetime: {
            metric:["audience_locale", "audience_country", "audience_city", "audience_gender_age", ],//"online_followers", <- not working
            scroll:{
                start:1,
                end:1,
                scroll:1,
            }
        }
    });

    // get the daily value for followers (we can't go back in time, but have to manually get the data)
    const option = {
        access_token: identity.access_token,
        fields: 'followers_count,ig_id'
    }
    try {
        let answer = await graphget(option, "/" + page)
        instagram_data.day['followers_count'] = [{ value: answer.followers_count, end_time: new Date().toISOString()}]

        // manually set the internal mapping for this metric as well
        internal_type_mapping.instagram.followers_count = 'follower_count'
        
    } catch (error) {
        console.log('error', error)
        
    }


    // get the last 100 instagram posts with insights. For instagram we ask the /media endpoint, for Facebook /published_posts
    const media_data = await instagramMedia(page, identity.access_token, 'instagram')
    const story_data = await instagramMedia(page, identity.access_token, 'instagram_stories')
    instagram_data.media = media_data.concat(story_data)
    
    
    
    // create the elasticsearch body
    const elastic_body = await elasticBody(instagram_data, aggregation_type_mapping, internal_type_mapping, 'instagram', user.user_id, page, brand_id )

    // last step: push everything to elasticsearch
    try {
        const resp_fbi = await client.bulk({body:elastic_body})
    } catch (error) {
        console.error(error) 
    }

    return {statusCode: 200}

}