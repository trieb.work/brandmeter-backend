import getMetric from '../lib/googlemybusiness'
import elasticBody from '../lib/elasticBody'
import client from '../lib/elasticConnect'



export default async function getGoogleMyBusiness(page, user, identity, days, brand_id) {

    if(!identity.refresh_token) throw new Error('No google refresh token found! Can\'t proceed')

    const internal_type_mapping = {

        googlemybusiness: {
            LOCAL_POST_ACTIONS_CALL_TO_ACTION: "cta_clicks",
            ACTIONS_PHONE: "phone_call_clicks",
            ACTIONS_WEBSITE: "website_clicks",
            ACTIONS_DRIVING_DIRECTIONS: "get_directions_clicks"
        }

    }

    const aggregation_type_mapping = {

        googlemybusiness: {
            ACTIONS_WEBSITE: "total_actions",
            ACTIONS_PHONE: "total_actions",
            ACTIONS_DRIVING_DIRECTIONS: "total_actions",
            LOCAL_POST_ACTIONS_CALL_TO_ACTION: "total_actions",

            VIEWS_MAPS: "impressions",
            VIEWS_SEARCH: "impressions"
        }
    }


    const googlemybusiness_data = await getMetric(page, user, identity, {
        day: {
            timeRangeName: 'AGGREGATED_DAILY',
            metric: Object.keys(aggregation_type_mapping.googlemybusiness),
            scroll: {
                start: 1,
                end: days,
                scroll: 30,
            }
        }
    })


    // last step: push everything to elasticsearch
    try {
        const elastic_body = await elasticBody(googlemybusiness_data, aggregation_type_mapping, internal_type_mapping, 'googlemybusiness', user.user_id, page, brand_id)

        // const resp_fbi = await client.bulk({
        //     body: elastic_body
        // })

    } catch (error) {
        console.error(error)
        return {statusCode: 500, error: error}
    }

    return {statusCode: 200}




}