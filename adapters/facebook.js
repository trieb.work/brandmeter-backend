import getMetric from '../lib/fbgraph'
import client from '../lib/elasticConnect'
import graphget from '../lib/fbgraphHelper'
import elasticBody from '../lib/elasticBody'
import facebookMedia from '../lib/fbgraphMedia'


// this is the wrapper function you call to get facebook metrics. 
export default async function getFacebook(page, user, identity, days, brand_id) {
   

    const internal_type_mapping = {
   
        facebook:{
            //no aggregation
            page_fans: "follower_count",
            
            //impressions 
            page_impressions_paid: "impressions_paid",
            page_impressions_organic: "impressions_organic",

            // impressions unique. Used for calculation of the engagement rate
            page_impressions_paid_unique: "impressions_paid_unique",
            page_impressions_organic_unique: "impressions_organic_unique",

            //total_actions
            page_cta_clicks_logged_in_total: "cta_clicks",
            page_call_phone_clicks_logged_in_unique: "phone_call_clicks",
            page_get_directions_clicks_logged_in_unique: "get_directions_clicks",
            page_website_clicks_logged_in_unique: "website_clicks",

            // engagement
            page_post_engagements: "post_engagements",

            page_engaged_users: "engaged_users"
        
        },
    }
    const aggregation_type_mapping = {

        facebook:{
            page_cta_clicks_logged_in_total: "total_actions",
            page_call_phone_clicks_logged_in_unique: "total_actions",
            page_get_directions_clicks_logged_in_unique: "total_actions",
            page_website_clicks_logged_in_unique: "total_actions",

            page_impressions_paid_unique: "impressions_unique",
            page_impressions_organic_unique: "impressions_unique",

            page_impressions_paid: "impressions",
            page_impressions_organic: "impressions",

            page_fans: "follower_count",

            page_post_engagements: "total_engagements"
        }
    }

    // get a Page Access_Token from our "normal" access_token (facebook is stupid)
    const temp = await graphget({access_token: identity.access_token, fields: 'access_token'}, "/" + page)

    const facebook_data = await getMetric(page, temp.access_token, {  //facebook id
        //day, week, days_28, month, lifetime
        day: {
            metric: Object.keys(internal_type_mapping.facebook),
            scroll:{
                start:1,
                end:days,
                scroll:30,
            }
        },
        lifetime: {
            metric:["post_activity", ],
            scroll:{
                start:1,
                end:1,
                scroll:1,
            }
        }
    });

    // get the last 100 facebook posts with insights. For instagram we ask the /media endpoint, for Facebook /published_posts
    const media_data = await facebookMedia(page, temp.access_token, 'facebook')
    facebook_data.media = media_data

    // create the elasticsearch body
    const elastic_body = await elasticBody(facebook_data, aggregation_type_mapping, internal_type_mapping, 'facebook', user.user_id, page, brand_id )

    console.log(elastic_body)

    // last step: push everything to elasticsearch
    try {
        const resp_fbi = await client.bulk({body:elastic_body})
        
    } catch (error) {
        console.error(error) 
    }
   
    return {statusCode: 200}


 
}