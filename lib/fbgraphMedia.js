import graphget from './fbgraphHelper'
import * as Parallel from 'async-parallel' 
import * as Sentry from '@sentry/node';
import { s3upload, createBucket } from './s3mediaupload'


export default async (insightId, access_token, application) => {
    const specifics = {
        instagram: {
            endpoint: '/media',
            metric: 'engagement,impressions,reach',
            fields: 'id,caption,timestamp,media_type,media_url,permalink,thumbnail_url'
        },
        instagram_stories: {
            endpoint: '/stories',
            metric: 'impressions,reach,exits,replies,taps_forward,taps_back',
            fields: 'id,caption,timestamp,media_type,media_url,thumbnail_url'
        },
        facebook: {
            endpoint: '/published_posts',
            metric: 'post_engaged_users,post_impressions,post_impressions_unique,post_engagements',
            fields: 'id,created_time,message,permalink_url,full_picture,attachments{media_type}'
        }
    }
    try {
        let option = {
            access_token, 
            fields: specifics[application].fields,
            limit: 100
        }
        // get all post IDs and metadata. We already make some normalization here.
        let res = await graphget(option, "/" + insightId + specifics[application].endpoint)
        res = res.data.map(post => {
            post.post_id = post.id
            delete post.id
            // this is just for facebook
            if(post.created_time){
                if(!post.message) return null
                post.caption = post.message
                post.permalink = post.permalink_url
                post.media_url = post.full_picture
                post.media_type = post.attachments.data[0].media_type
                delete post.attachments
                delete post.full_picture
                delete post.message
                delete post.permalink_url
            }
            // this is just for instagram
            if(post.timestamp){
                post.media_type = InstaMediaType(post.media_type)
                post.created_time = post.timestamp
                delete post.timestamp
            }
            if(post.thumbnail_url) post.media_url = post.thumbnail_url
            if(application === 'instagram_stories') post.post_type = 'story'
            return post
        })
        res = res.filter(x => x !== null )

        // now we need to make one request per media item to get insights
        // we also use the parallel / async loop to upload the media item to our s3 storage
        option = {access_token,metric: specifics[application].metric}
        // create the storage bucket if not already there
        await createBucket()
        await Parallel.each(res, async item => {
            let result
            try {
                result = await graphget(option, '/' + item.post_id + '/insights')
            } catch (error) {
                // errors that are okay to exist
                if ( error.error_subcode === 2108006){
                    result = null
                } else {
                    console.error('Error getting post insights', error)
                    Sentry.captureException(error)
                    result = null
                }
       
            }
            if(result) result.data.map(metric => {
                switch (metric.name) {
                    case 'engagement':
                        item.engagement = metric.values[0].value
                        break
                    case 'impressions':
                        item.impressions = metric.values[0].value
                        break
                    case 'reach':
                        item.impressions_unique = metric.values[0].value
                        break
                    case 'replies':
                        item.replies = metric.values[0].value
                        break
                    case 'post_engaged_users':
                        item.engaged_users =  metric.values[0].value
                        break
                    case 'post_impressions':
                        item.impressions = metric.values[0].value
                        break
                    case 'post_engagements':
                        item.engagement = metric.values[0].value
                        break    
                    case 'post_impressions_unique':
                        item.impressions_unique = metric.values[0].value
                        break
                }
                // calculate the enagegement rate. We do it now the same for insta and facebook
                item.engagement_rate = item.engagement / item.impressions
               
                

            })
            // upload the media item
            try {
                if(item.media_url) await s3upload(item.media_url, item.post_id)
                
            } catch (error) {
                console.log('error upload media files', error)
                console.log('media url', item.media_url, 'post id', item.post_id)
                Sentry.captureException(error);
                
            } 

        },8)
     

        return res

        
    } catch (error) {
        console.error('Error getting FBGraph posts', error);
        Sentry.captureException(error);
        
    }


}


const InstaMediaType = (media_type) => {
    const media = {
        CAROUSEL_ALBUM: 'album',
        IMAGE: 'photo',
        VIDEO: 'video'
    }
    return media[media_type]

}