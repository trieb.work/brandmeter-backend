import {OAuth2Client} from 'google-auth-library'


export default async(identity) => {
    const oAuth2Client = new OAuth2Client(
        process.env.GOOGLE_CLIENT_ID,
        process.env.GOOGLE_CLIENT_SECRET
    )
    await oAuth2Client.setCredentials({refresh_token: identity.refresh_token, access_token: identity.access_token} )

    return oAuth2Client
}