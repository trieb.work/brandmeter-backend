import * as graph from 'fbgraph'

export default function graphget(params, url) {

    return new Promise(function (resolve, reject) {
       
        graph.setVersion("5.0")
        graph.get(url, params, function (err, res) {
            if (err) return reject(err)
            else resolve(res)
        });
    });
}