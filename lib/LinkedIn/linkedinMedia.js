import axios from 'axios'
import moment from 'moment'
import * as Parallel from 'async-parallel'
import { s3upload, createBucket } from '../s3mediaupload' 
import * as Sentry from '@sentry/node';



export default async (page, access_token) => {

    const organization = encodeURIComponent('urn:li:organization:'+page)

    // get the last 100 post with some meta information
    try {
        let result = await axios({
            method: 'get',
            url: `https://api.linkedin.com/v2/ugcPosts?q=authors&authors=List(${organization})`,
            params: {
                count: 100
            },
            headers: {
                'Authorization': 'Bearer '+access_token,
                'X-Restli-Protocol-Version': '2.0.0'
            }
        })
        if(typeof result.data.elements !== "object" ){
            throw new Error('We got no valid response from the linkedin API')
        }

        result = result.data.elements.map(post => {
            post.post_id = post.id
            post.permalink = post.activity ? 'https://www.linkedin.com/feed/update/'+post.activity : 'https://www.linkedin.com/feed/update/'+post.id
            post.caption = post.specificContent['com.linkedin.ugc.ShareContent'].shareCommentary.text
            if(post.caption.length < 1) post.caption = 'No Caption'
            post.created_time = moment(post.created.time).toISOString()
            post.media_type = MediaTypes(post.specificContent['com.linkedin.ugc.ShareContent'].shareMediaCategory)
            
            if(post.specificContent['com.linkedin.ugc.ShareContent'].media.length > 0){
                let media = post.specificContent['com.linkedin.ugc.ShareContent'].media
                if(media[0].thumbnails.length > 0 ) post.media_url = media[0].thumbnails[0].url
            }
            if(post.specificContent['com.linkedin.ugc.ShareContent'].shareFeatures.hashtags){
                let array = post.specificContent['com.linkedin.ugc.ShareContent'].shareFeatures.hashtags
                post.hashtags = array.map(element => {
                    element = element.split(':')[3]
                    element = element.replace(/AeS/,'ü')
                    return element
                })
            } 
            
            delete post.created
            delete post.distribution
            delete post.serviceProvider
            delete post.edited
            delete post.owner
            delete post.activity
            delete post.lastModified
            delete post.id
            delete post.lifecycleState
            delete post.visibility
            delete post.contentCertificationRecord
            delete post.author
            delete post.versionTag
            delete post.origin
            delete post.firstPublishedAt
            delete post.specificContent
            delete post.ugcOrigin

            return post
        })


        // create the storage bucket if not already there
        await createBucket()
        // now add insights
        await Parallel.each(result, async item => {

            let params = {
                q: 'organizationalEntity',
                organizationalEntity: 'urn:li:organization:'+page,
                
            }
            item.post_id.includes('ugcPost') ? params.ugcPosts = item.post_id : params.shares = item.post_id
            let result = await axios({
                method: 'get',
                url: 'https://api.linkedin.com/v2/organizationalEntityShareStatistics',
                params: params,
                headers: {
                    'Authorization': 'Bearer '+access_token
                }
            })
            result = result.data.elements[0].totalShareStatistics
            item.impressions = result.impressionCount
            item.likes = result.likeCount
            item.comments = result.commentCount
            item.engagement_rate = result.engagement
            item.clicks = result.clickCount
            item.shares = result.shareCount
            item.engagement = result.shareCount + result.clickCount + result.likeCount + result.commentCount

            // upload the media item
            if (item && item.media_url) await s3upload(item.media_url, item.post_id)


        },5)


        return result


        
    } catch (error) {
        console.error('Error getting linkedin media', error)
        Sentry.captureException(error);
        
        
    }

   




}

const MediaTypes = (media_type) => {
    const media = {
        IMAGE: 'photo',
        VIDEO: 'video',
        ARTICLE: 'article',
        CAROUSEL: 'album',
        NONE: 'repost'

    }
    return media[media_type]

}