import axios from 'axios';
import moment from 'moment';
import * as Sentry from '@sentry/node';


export default async(page, token, options) => {


    // calculate the timestamps for the time interval. We need unix timestamps in milliseconds
    const start = moment().subtract(options.day.scroll.end, 'days').valueOf()
    const end = moment().valueOf()
    const metrics = options.day.metric

    try {
        let result = await axios({
            method: 'get',
            url: 'https://api.linkedin.com/v2/organizationalEntityShareStatistics',
            params:{
                q: 'organizationalEntity',
                organizationalEntity: 'urn:li:organization:'+page,
                'timeIntervals.timeGranularityType': 'DAY',
                'timeIntervals.timeRange.start':start,
                'timeIntervals.timeRange.end':end,
                start: 10,
                count:30
            },
            headers: {
                'Authorization': 'Bearer '+token
            }
    
        })
    
        if(typeof result.data.elements !== "object" ){
            throw new Error('We got no valid response from the linkedin API')
        }
        
        let response = {day: {}}
        for(const metric of metrics){
            response.day[metric]= []
        }
    
        result = result.data.elements
        result.map( dayValue => { 
            metrics.map(metric =>{
                // write the value 
                dayValue.totalShareStatistics[metric] ? response.day[metric] =  [...response.day[metric], {value: dayValue.totalShareStatistics[metric], end_time: moment(dayValue.timeRange.end).toISOString()}] : ''
            })
    
        })

        let follower = await axios({
            method: 'get',
            url: 'https://api.linkedin.com/v2/networkSizes/'+'urn:li:organization:'+page,
            params: {
                edgeType: 'CompanyFollowedByMember'
            },
            headers: {
                'Authorization': 'Bearer '+token
            }
        })

        if(typeof follower.data !== "object" ){
            throw new Error('We got no valid response from the linkedin API')
        }


        response.day.CompanyFollowedByMember =  [{ value: follower.data.firstDegreeSize, end_time: new Date().toISOString()}]



        
        return response
        


    } catch (error) {

        console.error('Error in the LinkedIn lib', error)
        Sentry.captureException(error);
        
    }



    




}