const ManagementClient = require('auth0').ManagementClient;


export default new ManagementClient({
    clientId: process.env.AUTH0_MANAGEMENT_CLIENTID,
    clientSecret: process.env.AUTH0_MANAGEMENT_CLIENTSECRET,
    domain: process.env.AUTH0_DOMAIN,
    scope: "read:users read:user_idp_tokens update:users"
});