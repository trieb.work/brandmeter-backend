import AWS from 'aws-sdk'
import axios from 'axios'
import * as Sentry from '@sentry/node';



const endpoint = new AWS.Endpoint(process.env.S3_URL);
const s3 = new AWS.S3({
    endpoint: endpoint,
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
    s3ForcePathStyle: true,
    region: 'nue1'
});

export async function s3upload(mediaurl, mediaID) {

    // first check, if the file already exist. If yes, we skip the upload.
    let params = {
        Bucket: process.env.S3_BUCKET_NAME,
        Key: mediaID+'.jpg'
    }
    let exist = true
    try {

        await s3.getObjectAcl(params).promise()
        
    } catch (error) {

        if (error.code === 'NoSuchKey') exist = false
        
    }

    if(!exist){
         // start the download stream
         let stream = await axios({
            url: mediaurl,
            method: 'GET',
            responseType: 'stream'
        })
        params.Body = stream.data
        params.ContentType = stream.headers['content-type']
        let result = await s3.upload(params).promise()


    } else{
        return true
    }


  


}

export async function createBucket(){
    const params = {
        Bucket: process.env.S3_BUCKET_NAME,
        ACL: 'private'
    }
    try {

        await s3.createBucket(params).promise()

    } catch (error) {

        if (error.statusCode === 409) {
            
        } else {
            console.error('Error creating bucket', error)
            Sentry.captureException(error)
        }

    }

    return true


}