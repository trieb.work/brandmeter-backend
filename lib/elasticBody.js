export default async function (data, aggregation_type_mapping, internal_type_mapping, network, user_id, page, brand_id) {


    let elasticdayout = {};

    for (let time of Object.keys(data).filter(k => k === 'day')) {
        for (let metric in data[time]) {
            for (let value of data[time][metric]) {
                // if we have empty array values we just skip them
                if (!value) continue
                let id = user_id + '_' + value.end_time.substr(0, 10) + '_' + brand_id + '_' + network + metric
                if (typeof value.value === 'object') {

                }
                if (!elasticdayout[id]) elasticdayout[id] = {
                    application: network,
                    timestamp: value.end_time,
                    metric_name: metric,
                    metric_value: value.value,
                    aggregated_type: aggregation_type_mapping[network] && aggregation_type_mapping[network][metric] ? aggregation_type_mapping[network][metric] : null,
                    internal_type: internal_type_mapping[network] && internal_type_mapping[network][metric] ? internal_type_mapping[network][metric] : null,
                    user_id: user_id,
                    page_id: page,
                    brand_id
                };
            }
        }
    }

    let elasticlifeout = {};
    for (let metric in data.lifetime) {
        let value = data.lifetime[metric][0]
        // console.log(value.value)
        for (let key in value.value) {
            let id = user_id + '_' + value.end_time.substr(0, 10) + key + metric
            if (!elasticlifeout[id]) elasticlifeout[id] = {
                timestamp: value.end_time
            };
            elasticlifeout[id].value = value.value[key]
            elasticlifeout[id].dimension_value = key
            elasticlifeout[id].dimension_key = metric
            elasticlifeout[id].specific_type = metric
            elasticlifeout[id].general_type = aggregation_type_mapping[network][metric] || "none"
            elasticlifeout[id].application = network
            elasticlifeout[id].user_id = user_id
            elasticlifeout[id].page_id = page
            elasticlifeout[id].brand_id = brand_id
        }
    }

    let media = {}

    if (data.media) {
        for (let post of data.media) {
            let id = user_id + '_' + post.created_time.substr(0, 10) +  brand_id + '_' + network + post.post_id
            if (!media[id]) media[id] = {
                timestamp: post.created_time
            }
            media[id].application = network
            media[id].impressions = post.impressions
            media[id].impressions_unique = post.impressions_unique
            media[id].engagement = post.engagement
            media[id].engagement_rate = post.engagement_rate
            media[id].engaged_users = post.engaged_users
            media[id].permalink = post.permalink
            media[id].post_id = post.post_id
            media[id].media_url = post.media_url
            media[id].caption = post.caption
            media[id].media_type = post.media_type
            media[id].user_id = user_id
            media[id].page_id = page
            media[id].caption = post.caption
            media[id].shares = post.shares
            media[id].clicks = post.clicks
            media[id].likes = post.likes
            media[id].comments = post.comments
            media[id].hashtags = post.hashtags
            media[id].post_type = post.post_type
            media[id].brand_id = brand_id

        }

    }





    /*
{
    "timestamp" : "2019-12-11T08:00:00+0000",
    "value" : 24,
    "dimension_key" : "locale",
    "dimension_value" : "it_IT",
    "specific_type" : "audience_locale",
    "general_type" : "none",
    "application" : "instagram"
}
    */

    // console.log(Object.values(elasticdayout).filter(o => o.network === 'facebook'))
    // process.exit()
    let body_day = Object.keys(elasticdayout).map(key => ([{
            index: {
                "_index": 'brandmeter-elasticdayout',
                "_id": key
            }
        },
        elasticdayout[key]
    ]))

    let body_life = Object.keys(elasticlifeout).map(key => ([{
            index: {
                "_index": 'brandmeter-elasticlifeout',
                "_id": key
            }
        },
        elasticlifeout[key]
    ]))
    let body_media = Object.keys(media).map(key => ([{
            index: {
                "_index": 'brandmeter-posts',
                "_id": key
            }
        },
        media[key]
    ]))
    let body = (body_day.concat(body_life).concat(body_media)).flat(1)


    //let body = (body_day.concat(body_life)).flat(1)
    return body;

}